#! /usr/local/bin/python

import sys
import numpy
import scipy
import os
import copy

class FileEntry:
    now = 0
    offset = 0

class Experiment:
    average = 0.0
    stddev = 0.0
    lost = 0
    outliers = 0
    packet_total = 0
    records = []

class ExperimentSet:
    name = ""
    experiments = []
    records = []
    lost = 0
    outliers = 0
    packet_total = 0
    average = 0.0
    stddev = 0.0

def parse_text_file(name):
    filename = name
    start = 0
    entry = 0
    lost_count = 0
    packet_total = 0
    offset_reset_trigger = 3.2e4 # 1000microsec
    data = []
    (filename_name, filename_ext) = os.path.splitext(filename)
    f = open(filename, 'r')
    lines = f.readlines()
    f.close()
    output_csv = open(filename_name + ".csv", "w")
    output_csv.write("time,offset\n")
    reset_happening = False
    keep_alive_sent = False
    keep_alive_count = 0
    lost_count = 0
    for i in range(len(lines)):
        if "timer" in lines[i]:
            start = i
            break
    for i in range(start, len(lines)):
        if "TSTP Sensor test" in lines[i]:
            reset_happening = True
        elif "sink" in lines[i]:
            reset_happening = False
        elif "keep_alive():keep_alive" in lines[i]:
            packet_total += 1
            if not keep_alive_sent:
                keep_alive_sent = True
            else:
                keep_alive_count += 1
                lost_count += 1
        elif "timer" in lines[i]:
            line = lines[i]
            line = line.strip().split(" ")
            offset = int(line[-1])
            if abs(offset) > offset_reset_trigger:
                print "** Reset likely happened at line " + str(i) + ", entry " + str(entry) + ", after " + str(keep_alive_count) + " lost packets resulting in offset " + str(offset)
            data.append(FileEntry())
            data[entry].offset = offset
            keep_alive_sent = False
            keep_alive_count = 0
        elif "now()" in lines[i]:
            line = lines[i]
            line = line.strip().split(" ")
            now = int(line[-1])
            data[entry].now = now
            entry += 1
    if data[-1].now == 0:
        del data[-1]
    while data[-1].now == 0 and data[-1].offset == 0:
        del data[-1]
    valid_abs = []
    sum_of_valid_abs = 0
    sum_of_valid_values = 0
    outliers = 0
    valid_values = 0
    for i in range(0, len(data)):
        line = str(data[i].now) + "," + str(data[i].offset) + "\n"
        if abs(data[i].offset) < offset_reset_trigger:
            valid_values += 1
            sum_of_valid_abs += abs(data[i].offset)
            valid_abs.append(abs(data[i].offset))
            sum_of_valid_values += data[i].offset
        else:
            outliers += 1
        output_csv.write(line)
    output_csv.close()
    if valid_values == 0:
        print "Error: only invalid values were logged."
    abs_average = numpy.average(valid_abs)
    stddev = numpy.std(valid_abs)
    print "Valid values: " + str(valid_values)
    print "Average of absolute offsets = " + str(abs_average)
    print "Standard deviation over absolute offsets = " + str(stddev)
    return {"valid_abs" : valid_abs, "abs_average" : abs_average, "stddev" : stddev, "lost_count" : lost_count, "outliers" : outliers, "packet_total" : packet_total}

dir_list = os.listdir('.')
files = []
for filename in dir_list:
    if not filename.startswith('.') and filename.endswith('.txt') and (filename.startswith('15min_') or filename.startswith('20min_')):
        files.append(filename)
files.sort()
experiment_sets = {}
for filename in files:
    print "***** Now parsing " + filename + " *****"
    name = filename.split('_')
    if len(name) != 3:
        continue
    experiment_type = name[1]
    experiment_count = ""
    for i in range(len(name[2])):
        if name[2][i] == '.':
            break
        experiment_count = experiment_count + name[2][i]
    experiment_count = int(experiment_count)
    result = parse_text_file(filename)
    experiment = Experiment()
    experiment.records = result["valid_abs"]
    experiment.average = result["abs_average"]
    experiment.stddev = result["stddev"]
    experiment.lost = result["lost_count"]
    experiment.outliers = result["outliers"]
    experiment.packet_total = result["packet_total"]
    if experiment_type in experiment_sets:
        experiment_sets[experiment_type].experiments.append(experiment)
        experiment_sets[experiment_type].records += experiment.records
    else:
        experiment_sets[experiment_type] = ExperimentSet()
        experiment_sets[experiment_type].name = experiment_type
        experiment_sets[experiment_type].experiments = [experiment]
        experiment_sets[experiment_type].records = experiment.records
    experiment_sets[experiment_type].lost += experiment.lost
    experiment_sets[experiment_type].outliers += experiment.outliers
    experiment_sets[experiment_type].packet_total += experiment.packet_total

for key in experiment_sets.keys():
    experiment_sets[key].average = numpy.average(experiment_sets[key].records)
    experiment_sets[key].stddev = numpy.std(experiment_sets[key].records)
    experiment_sets[key].lost = float(experiment_sets[key].lost) / experiment_sets[key].packet_total
    experiment_sets[key].outliers = float(experiment_sets[key].outliers) / experiment_sets[key].packet_total

sorted_output = []
print "\n*** Results from sets"
for key in experiment_sets.keys():
    experiment_set = experiment_sets[key]
    text_output = ""
    text_output += "In group : " + experiment_set.name + "\n"
    text_output += "    Average: " + str(experiment_set.average) + "\n"
    text_output += "    Std dev: " + str(experiment_set.stddev) + "\n"
    text_output += "    Loss: " + "%.3f" % round(100 * experiment_set.lost, 3) + "%\n"
    text_output += "    Outliers: " + "%.3f" % round(100 * experiment_set.outliers, 3) + "%\n"
    sorted_output.append([key, text_output])

sorted_output = sorted(sorted_output, key = lambda group: group[0])
for group in sorted_output:
    print group[1]


# TODO: remove outliers
# TODO: add real average
