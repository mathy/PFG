#! /usr/local/bin/python

from statsmodels import robust
import copy
import numpy
import os
import scipy
import sys

class FileEntry:
    now = 0
    offset = 0

class Experiment:
    average = 0.0
    real_average = 0.0
    stddev = 0.0
    mad = 0.0
    lost = 0
    reset_outliers = 0
    over_threshold = 0
    packet_total = 0
    records = []

class ExperimentSet:
    name = ""
    experiments = []
    records = []
    lost = 0
    reset_outliers = 0
    over_threshold = 0
    packet_total = 0
    average = 0.0
    real_average = 0.0
    stddev = 0.0
    mad = 0.0

def parse_text_file(name):
    filename = name
    start = 0
    entry = 0
    lost_count = 0
    packet_total = 0
    outlier_stddev_factor = 0
    offset_reset_trigger = 3.2e4 # 1000microsec
    threshold = 3840 # 1microsec
    over_threshold = 0
    data = []
    (filename_name, filename_ext) = os.path.splitext(filename)
    f = open(filename, 'r')
    lines = f.readlines()
    f.close()
    output_csv = open(filename_name + ".csv", "w")
    output_csv.write("time,offset\n")
    reset_happening = False
    keep_alive_sent = False
    keep_alive_count = 0
    lost_count = 0
    for i in range(len(lines)):
        if "timer" in lines[i]:
            start = i
            break
    for i in range(start, len(lines)):
        if "TSTP Sensor test" in lines[i]:
            reset_happening = True
        elif "sink" in lines[i]:
            reset_happening = False
        elif "keep_alive():keep_alive" in lines[i]:
            packet_total += 1
            if not keep_alive_sent:
                keep_alive_sent = True
            else:
                keep_alive_count += 1
                lost_count += 1
        elif "timer" in lines[i]:
            line = lines[i]
            line = line.strip().split(" ")
            offset = int(line[-1])
            if abs(offset) > offset_reset_trigger:
                print "** Reset likely happened at line " + str(i) + ", entry " + str(entry) + ", after " + str(keep_alive_count) + " lost packets resulting in offset " + str(offset)
            data.append(FileEntry())
            data[entry].offset = offset
            keep_alive_sent = False
            keep_alive_count = 0
        elif "now()" in lines[i]:
            line = lines[i]
            line = line.strip().split(" ")
            now = int(line[-1])
            data[entry].now = now
            entry += 1
    if data[-1].now == 0:
        del data[-1]
    while data[-1].now == 0 and data[-1].offset == 0:
        del data[-1]
    valid_abs = []
    valid_real_values = []
    sum_of_valid_abs = 0
    sum_of_valid_values = 0
    reset_outliers = 0
    valid_values = 0
    for i in range(0, len(data)):
        line = str(data[i].now) + "," + str(data[i].offset) + "\n"
        if abs(data[i].offset) < offset_reset_trigger:
            valid_values += 1
            sum_of_valid_abs += abs(data[i].offset)
            valid_abs.append(abs(data[i].offset))
            sum_of_valid_values += data[i].offset
            if abs(data[i].offset) > threshold:
                over_threshold += 1
        else:
            reset_outliers += 1
        output_csv.write(line)
    output_csv.close()
    if valid_values == 0:
        print "Error: only invalid values were logged."
    abs_average = numpy.average(valid_abs)
    stddev = numpy.std(valid_abs)
    mad = robust.mad(valid_abs, c = 1.0)
    if outlier_stddev_factor > 0.0:
        valid_abs = [abs_offset for abs_offset in valid_abs if ((abs_offset > abs_average - outlier_stddev_factor * stddev) and (abs_offset < abs_average + outlier_stddev_factor * stddev))]
        abs_average = numpy.average(valid_abs)
        stddev = numpy.std(valid_abs)
        mad = robust.mad(valid_abs, c = 1.0)
    print "Valid values: " + str(valid_values)
    print "Average of absolute offsets = " + str(abs_average)
    print "Standard deviation over absolute offsets = " + str(stddev)
    return {"valid_abs" : valid_abs, "abs_average" : abs_average, "stddev" : stddev, "lost_count" : lost_count, "reset_outliers" : reset_outliers, "packet_total" : packet_total, "mad" : mad, "over_threshold" : over_threshold}

dir_list = os.listdir('.')
files = []
for filename in dir_list:
    if not filename.startswith('.') and filename.endswith('.txt') and (filename.startswith('15min_') or filename.startswith('20min_')):
        files.append(filename)
files.sort()
experiment_sets = {}
for filename in files:
    print "***** Now parsing " + filename + " *****"
    name = filename.split('_')
    if len(name) != 3:
        continue
    experiment_type = name[1]
    experiment_count = ""
    for i in range(len(name[2])):
        if name[2][i] == '.':
            break
        experiment_count = experiment_count + name[2][i]
    experiment_count = int(experiment_count)
    result = parse_text_file(filename)
    experiment = Experiment()
    experiment.records = result["valid_abs"]
    experiment.average = result["abs_average"]
    experiment.stddev = result["stddev"]
    experiment.mad = result["mad"]
    experiment.lost = result["lost_count"]
    experiment.reset_outliers = result["reset_outliers"]
    experiment.packet_total = result["packet_total"]
    experiment.over_threshold = result["over_threshold"]
    if experiment_type in experiment_sets:
        experiment_sets[experiment_type].experiments.append(experiment)
        experiment_sets[experiment_type].records += experiment.records
        experiment_sets[experiment_type].over_threshold += experiment.over_threshold
    else:
        experiment_sets[experiment_type] = ExperimentSet()
        experiment_sets[experiment_type].name = experiment_type
        experiment_sets[experiment_type].experiments = [experiment]
        experiment_sets[experiment_type].records = experiment.records
    experiment_sets[experiment_type].lost += experiment.lost
    experiment_sets[experiment_type].reset_outliers += experiment.reset_outliers
    experiment_sets[experiment_type].packet_total += experiment.packet_total

for key in experiment_sets.keys():
    experiment_sets[key].average = numpy.average(experiment_sets[key].records)
    experiment_sets[key].stddev = numpy.std(experiment_sets[key].records)
    experiment_sets[key].mad = robust.mad(experiment_sets[key].records, c = 1.0)
    experiment_sets[key].lost = float(experiment_sets[key].lost) / experiment_sets[key].packet_total
    experiment_sets[key].reset_outliers = float(experiment_sets[key].reset_outliers) / len(experiment_sets[key].records)

sorted_output = []
print "\n*** Results from sets"
for key in experiment_sets.keys():
    experiment_set = experiment_sets[key]
    text_output = ""
    text_output += "In group : " + experiment_set.name + "\n"
    text_output += "    Average: " + str(experiment_set.average) + "\n"
    text_output += "    Std dev: " + str(experiment_set.stddev) + "\n"
    text_output += "    MAD: " + str(experiment_set.mad) + "\n"
    text_output += "    Loss: " + "%.3f" % round(100 * experiment_set.lost, 3) + "%\n"
    text_output += "    Over 40ppm: " + "%.3f" % round(100 * float(experiment_set.over_threshold) / len(experiment_set.records), 3) + "%\n"
    # text_output += "    Outliers: " + "%.3f" % round(100 * experiment_set.reset_outliers, 3) + "%\n"
    sorted_output.append([key, text_output])

sorted_output = sorted(sorted_output, key = lambda group: group[0])
for group in sorted_output:
    print group[1]


# TODO: remove outliers
# TODO: add real average
